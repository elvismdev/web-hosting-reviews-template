<?php

/**
 * Content boxes
 */
add_shortcode( 'content_box', 'p_whr_content_box' );
function p_whr_content_box( $atts ) {
	extract( shortcode_atts( array(
		'bgcolor' => 'gray',	// Optional
		'bullet' => 'check-orange',	// Optional
		'border' => 'blue' // Optional
		), $atts ) );
	$html = '<div class="p-content-box ' . $border . '"><div class="p-content-box ' . $bullet . ' ' . $bgcolor . '">';
	return $html;
}

add_shortcode( 'x_content_box', 'p_whr_x_content_box' );
function p_whr_x_content_box() {
	$html = '</div></div>';
	return $html;
}


/**
 * Pros & Cons
 */
add_shortcode( 'pros_cons', 'p_whr_pros_cons' );
function p_whr_pros_cons(  ) {
	$html = '<div class="p-pros-cons">';
	return $html;
}

add_shortcode( 'x_pros_cons', 'p_whr_x_pros_cons' );
function p_whr_x_pros_cons(  ) {
	$html = '</div>';
	return $html;
}


/**
 * Pros & Cons Left Box
 */
add_shortcode( 'pros_cons_left', 'p_whr_pros_cons_left' );
function p_whr_pros_cons_left(  ) {
	$html = '<div class="p-pros-cons-box pleft">';
	return $html;
}

add_shortcode( 'x_pros_cons_left', 'p_whr_x_pros_cons_left' );
function p_whr_x_pros_cons_left(  ) {
	$html = '</div>';
	return $html;
}


/**
 * Pros & Cons Right Box
 */
add_shortcode( 'pros_cons_right', 'p_whr_pros_cons_right' );
function p_whr_pros_cons_right(  ) {
	$html = '<div class="p-pros-cons-box pright">';
	return $html;
}

add_shortcode( 'x_pros_cons_right', 'p_whr_x_pros_cons_right' );
function p_whr_x_pros_cons_right(  ) {
	$html = '</div>';
	return $html;
}


/**
 * Green Box
 */
add_shortcode( 'green_box', 'p_whr_green_box' );
function p_whr_green_box( $atts ) {
	extract( shortcode_atts( array(
		'heading' => 'The Heading Title',	// Optional
		'message' => 'The Message',	// Optional
		), $atts ) );
	$html = '<div class="p-green-box success icon"><i class="fa fa-check" aria-hidden="true"></i><div class="content"><p><strong>' . $heading . '</strong><br>' . $message . '</p></div></div>';
	return $html;
}


/**
 * Green Button
 */
add_shortcode( 'green_btn', 'p_whr_green_btn' );
function p_whr_green_btn(  ) {
	global $hReviewData;
	$html = '<p class="green-btn"><a title="Click here, visit ' . get_the_title() . ' online" href="' . get_post_permalink( $hReviewData['affiliate_link']->ID ) . '" target="_blank"><strong>Visit ' . get_the_title() . '</strong></a></p>';
	return $html;
}


/**
 * Coupon Button Box
 */
add_shortcode( 'coupon_box', 'p_whr_coupon_box' );
function p_whr_coupon_box( $atts ) {
	global $hReviewData;
	extract( shortcode_atts( array(
		'left_message' => 'The left message.',	// Optional
		'button_label' => 'Activate Coupon &amp; Buy Now',	// Optional
		), $atts ) );
	$html = '<div class="well clearfix p-coupon-box"><p>' . $left_message . '</p><a href="' . get_post_permalink( $hReviewData['affiliate_link']->ID ) . '" class="p-coupon-box simple large pull-right" target="_blank"><i class="fa fa-scissors fa-lg"></i> ' . $button_label . ' <i class="fa fa-chevron-circle-right"></i></a></div>';

	return $html;
}


/**
 * Icon Circle
 */
add_shortcode( 'icon_circle', 'p_whr_icon_circle' );
function p_whr_icon_circle(  ) {
	$html = '<span class="p-table-icon" style="font-size:13px !important;"><i class="fa fa-circle" aria-hidden="true" style="color:inherit !important;"></i></span>';
	return $html;
}