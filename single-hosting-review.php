<?php
/**
 * Single Hosting Review Page.
 *
 * @package Genesis\Templates
 * @author  Parenthesis Tech
 * @license GPL-2.0+
 * @link    https://parenthesis.io/
 */

/* * GLOBAL DATA FOR THIS PAGE * */
add_action( 'genesis_before', 'p_whr_hosting_review_global_data' );
function p_whr_hosting_review_global_data() {
    // DEFINE CUSTOM GLOBAL VARIABLE
	global $hReviewData;
	// GET AFFILIATE LINK DATA
	$affiliate_link = get_post( types_render_field( 'affiliate-link-id' ) );
	$affiliate_link_data = unserialize( get_post_meta( $affiliate_link->ID, 'thirstyData', true ) );
	$hReviewData = array(
        // AFFILIATE LINK
		'affiliate_link'      	=> $affiliate_link,
        // AFFILIATE LINK METADATA
		'affiliate_link_data' 	=> $affiliate_link_data,
		// AFFILIATE RAW LINK
		'hosting_main_url'		=> parse_url( $affiliate_link_data['linkurl'] ),
		// PRICE
		'price'					=> types_render_field( 'price' )
		);
}

// Force content-sidebar layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );

// Remove <p> tags from the excerpt.
remove_filter('the_excerpt', 'wpautop');


// Output Easy Social Share buttons.
add_action( 'genesis_before_entry', 'p_whr_output_ess' );
function p_whr_output_ess() {
	echo do_shortcode( '[ess_post share_type="text"]' );
}

/**
 * Custom Micro Data To Reviews Custom Post Type.
 */
add_filter( 'genesis_attr_entry', 'p_whr_hosting_review_microdata_schema' );
function p_whr_hosting_review_microdata_schema( $attributes ){
	$attributes['itemtype'] = 'http://schema.org/Review';
	return $attributes;
}

// Set the reviewBody Micro data
add_filter( 'genesis_attr_entry-content', 'p_whr_hosting_review_microdata_schema_entry_content' , 20 );
function p_whr_hosting_review_microdata_schema_entry_content( $attributes ) {
	$attributes['itemprop'] = 'reviewBody';
	return $attributes;
}

// Remove entry header where contains a post title and post meta.
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

// Quick Facts Box.
add_action( 'genesis_entry_header', 'p_whr_quick_facts_box' );
function p_whr_quick_facts_box() {
	global $hReviewData;
	$services = types_render_field( 'services', array( 'separator' => ', ' ) );
	$reviewed_by = types_render_field( 'reviewed-by' );
	$rating_value = types_render_field( 'rating' );
	$summary = types_render_field( 'summary', array( 'output' => 'raw' ) );
	?>
	<div class="quick-facts-box">
		<div class="quick-facts-box-wrapper">
			<div>
				<h3><span class="title" itemprop="name"><?php the_title(); ?></span></h3>
			</div>
			<div class="clear"></div>

			<?php if ( $reviewed_by ): ?>
				<div><?php _e( 'Reviewed by', 'hosting-reviews' ) ?>: <span class="reviewer author"><span class="author" itemprop="author"><?php echo $reviewed_by; ?></span></span></div>
			<?php endif; ?>

			<?php if ( $rating_value ): ?>
				<dl>
					<dt><?php _e( 'Rating (out of 10)', 'hosting-reviews' ) ?>:</dt>
					<dd>
						<div class="result rating" itemtype="http://schema.org/Rating" itemscope="" itemprop="reviewRating">
							<meta content="1" itemprop="worstRating">
							<meta content="<?php echo $rating_value; ?>" itemprop="ratingValue">
							<meta content="<?php echo $rating_value; ?>" itemprop="bestRating">
							<span class="rating-value"><?php echo $rating_value; ?></span>
						</div>
					</dd>
				</dl>
				<div class="clear"></div>
			<?php endif; ?>

			<div class="last_update_meta"><?php _e( 'Last updated date', 'hosting-reviews' ) ?>: <span class="modified rating_date" itemprop="dateModified"><span title="<?php the_modified_date(); ?>"><?php the_modified_date(); ?></span></span></div>

			<?php if ( $hReviewData['affiliate_link'] ): ?>
				<dl>
					<dt><?php _e( 'URL', 'hosting-reviews' ) ?>:</dt>
					<dd>
						<span><?php echo do_shortcode( '[thirstylink linkid="' . $hReviewData['affiliate_link']->ID . '" linktext="' . $hReviewData['hosting_main_url']['host'] . '" title=""]' ); ?></span>
					</dd>
				</dl>
				<div class="clear"></div>
			<?php endif; ?>

			<?php if ( $services ): ?>
				<dt><?php _e( 'Services', 'hosting-reviews' ) ?>:</dt>
				<dd><?php echo $services; ?></dd>
				<div class="clear"></div>
			<?php endif; ?>

			<?php if ( $hReviewData['price'] ): ?>
				<dt><?php _e( 'Price', 'hosting-reviews' ) ?>:</dt>
				<dd><?php echo $hReviewData['price']; ?></dd>
				<div class="clear_space"></div>
			<?php endif; ?>

			<?php if ( $summary ): ?>
				<div class="hr"></div>
				<br>
				<b><?php _e( 'Summary', 'hosting-reviews' ) ?></b>
				<div class="description summary" itemprop="description">
					<p><span><?php echo $summary; ?></span></p>
				</div>
			<?php endif; ?>

			<?php if ( $hReviewData['affiliate_link'] ): ?>
				<div class="more-details-cta">
					<a itemprop="url" class="more-details-cta-btn" href="<?php echo get_post_permalink( $hReviewData['affiliate_link']->ID ); ?>" title="<?php echo $hReviewData['affiliate_link']->post_title; ?>" target="_blank" rel="nofollow"><?php _e( 'More Details', 'hosting-reviews' ) ?></a>
				</div>
				<div class="clear"></div>
			<?php endif; ?>
		</div>
	</div>
	<?php }

/**
 * Schema.org item reviewed.
 */
add_action( 'genesis_entry_content', 'p_whr_item_reviewed' );
function p_whr_item_reviewed() { ?>
<div itemtype="http://schema.org/Thing" itemscope="" itemprop="itemReviewed">
	<meta content="<?php the_title(); ?>" itemprop="name">
</div>
<?php }

/**
 * Sitcky Sidebar.
 */
add_action( 'genesis_before_sidebar_widget_area', 'p_whr_sticky_sidebar' );
function p_whr_sticky_sidebar() {
	global $hReviewData; ?>
	<section id="main-features-sidebar">

		<div class="widget-wrap">
			<h3 class="widgettitle first"><?php the_title(); ?></h3>
			<div class="widgetbody">

				<?php if ( has_post_thumbnail() ): ?>
					<a target="_blank" href="<?php echo get_post_permalink( $hReviewData['affiliate_link']->ID ); ?>">
						<?php the_post_thumbnail(); ?>
					</a>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( $hReviewData['hosting_main_url']['host'] ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Website', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo do_shortcode( '[thirstylink linkid="' . $hReviewData['affiliate_link']->ID . '" linktext="' . $hReviewData['hosting_main_url']['host'] . '" title=""]' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( $hReviewData['price'] ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Regular Price', 'hosting-reviews' ); ?></span>
						<span class="data-value"><del><?php echo $hReviewData['price']; ?></del></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( types_render_field( 'discount' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Discount', 'hosting-reviews' ); ?></span>
						<span class="data-value green"><?php echo types_render_field( 'discount' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( types_render_field( 'our-price' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Our Price', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'our-price' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( $hReviewData['affiliate_link']->ID ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Redeem Deal', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo do_shortcode( '[thirstylink linkid="' . $hReviewData['affiliate_link']->ID . '" linktext="' . __( 'Link Activation', 'hosting-reviews' ) . '" title=""]' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( types_render_field( 'money-back-guarantee' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Money Back Guarantee', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'money-back-guarantee' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

			</div>
		</div>

		<div class="widget-wrap">
			<h3 class="widgettitle"><?php _e( 'Features', 'hosting-reviews' ); ?></h3>
			<div class="widgetbody">

				<?php if ( types_render_field( 'free-domain' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Free Domain', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'free-domain' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( types_render_field( 'storage' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Storage', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'storage' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

			</div>
		</div>

		<div class="widget-wrap">
			<h3 class="widgettitle"><?php _e( 'Email', 'hosting-reviews' ); ?></h3>
			<div class="widgetbody">

				<?php if ( types_render_field( 'email-accounts' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Email Accounts', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'email-accounts' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( types_render_field( 'webmail' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Webmail', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'webmail' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

			</div>
		</div>

		<div class="widget-wrap">
			<h3 class="widgettitle"><?php _e( 'Customer Support', 'hosting-reviews' ); ?></h3>
			<div class="widgetbody">

				<?php if ( types_render_field( 'phone-support' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Phone Support', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'phone-support' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( types_render_field( 'live-chat' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Live Chat', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'live-chat' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

			</div>
		</div>

		<div class="widget-wrap">
			<h3 class="widgettitle"><?php _e( 'Visit', 'hosting-reviews' ); ?></h3>
			<div class="widgetbody">

				<?php if ( types_render_field( 'monthly-price' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Monthly Price', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'monthly-price' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( types_render_field( 'discount' ) ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Our Discount', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo types_render_field( 'discount' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

				<?php if ( $hReviewData['affiliate_link']->ID ): ?>
					<div class="data-out">
						<span class="data-label"><?php _e( 'Website', 'hosting-reviews' ); ?></span>
						<span class="data-value"><?php echo do_shortcode( '[thirstylink linkid="' . $hReviewData['affiliate_link']->ID . '" linktext="' . $hReviewData['hosting_main_url']['host'] . '" title=""]' ); ?></span>
					</div>
					<div class="clear"></div>
				<?php endif; ?>

			</div>
		</div>

	</section>
	<?php }

/**
 * Stick The Sidebar.
 */
add_action( 'genesis_after', 'p_whr_stick_the_sidebar' );
function p_whr_stick_the_sidebar() { ?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		var q2w3_sidebar_1_options = {
			"sidebar" : "sidebar",
			"margin_top" : 150,
			"margin_bottom" : 0,
			"screen_max_width" : 0,
			"width_inherit" : false,
			"widgets" : ['main-features-sidebar']
		};
		q2w3_sidebar(q2w3_sidebar_1_options);
		setInterval(function () { q2w3_sidebar(q2w3_sidebar_1_options); }, 1500);
	});
</script>
<?php }

genesis();
