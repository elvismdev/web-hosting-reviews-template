function getColor(pval) {
	if(pval >= 0.0 && pval < 0.1) {
		ratingColor = "#e67c73";
	} else if(pval >= 0.1 && pval < 0.2) {
		ratingColor = "#ec9170";
	} else if(pval >= 0.2 && pval < 0.3) {
		ratingColor = "#f2a46d";
	} else if(pval >= 0.3 && pval < 0.4) {
		ratingColor = "#f7b96a";
	} else if(pval >= 0.4 && pval < 0.5) {
		ratingColor = "#fdcc67";
	} else if(pval >= 0.5 && pval < 0.6) {
		ratingColor = "#ecd36a";
	} else if(pval >= 0.6 && pval < 0.7) {
		ratingColor = "#c7cd72";
	} else if(pval >= 0.7 && pval < 0.8) {
		ratingColor = "#a1c77a";
	} else if(pval >= 0.8 && pval < 0.9) {
		ratingColor = "#7cc182";
	} else if(pval >= 0.9) {
		ratingColor = "#57bb8a";
	}
	return ratingColor;
}
