<?php
/**
 *
 * @package Genesis\Templates
 * @author  Parenthesis Tech
 * @license GPL-2.0+
 * @link    https://parenthesis.io/
 */

//* Template Name: Home

/** Force full width content layout */
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );


/*
 * CTA Articles
 */
add_action( 'genesis_before_footer', 'p_whr_cta_articles', 1 );
function p_whr_cta_articles(  ) { ?>

<section class="block cta-articles-section">
	<div class="row-fluid">
		<div class="span2"></div>

		<?php
		$child_pages = get_pages(
			array(
				'child_of' => get_the_ID(),
				'sort_column' => 'post_date',
				'sort_order' => 'asc'
				)
			);

		foreach( $child_pages as $page ) {
			$link = get_page_link( $page->ID );
			$title = get_post_meta( $page->ID, 'wpcf-home-title', true );
			$excerpt = get_post_meta( $page->ID, 'wpcf-home-excerpt', true );
			?>

			<div class="span2">
				<p class="bw"><a href="<?php echo $link; ?>"><?php echo get_the_post_thumbnail( $page->ID, 'full' ); ?></a></p>
				<h3><?php echo $title; ?></h3>
				<p><?php echo $excerpt; ?></p>
				<a class="meteor-capsule" href="<?php echo $link; ?>"><?php _e( 'Click Here', 'hosting-reviews' ); ?></a>
			</div>

			<?php
		}
		?>

		<div class="span2"></div>
	</div>
</section>

<?php }


genesis();
