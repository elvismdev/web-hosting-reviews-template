<?php
/**
 *
 * @package Genesis\Templates
 * @author  Parenthesis Tech
 * @license GPL-2.0+
 * @link    https://parenthesis.io/
 */

//* Template Name: Hosting Reviews Archive

/** Force full width content layout */
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );


/*
 * Hostings Wrapper
 */
add_action( 'genesis_before_while', 'p_whr_top_rated_label' );
function p_whr_top_rated_label(  ) {
	echo '<span class="tag tag-top-rated">' . __( 'Top Rated', 'hosting-reviews' ) . '</span>';
}

/*
 * Custom Loop
 */
remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop
add_action( 'genesis_loop', 'p_whr_hosting_reviews' ); // Add custom loop
function p_whr_hosting_reviews() {
	// global $hReviewData;

	//* Use old loop hook structure if not supporting HTML5
	if ( ! genesis_html5() ) {
		genesis_legacy_loop();
		return;
	}

	if ( have_posts() ) :

		do_action( 'genesis_before_while' );
	while ( have_posts() ) : the_post();

	do_action( 'genesis_before_entry' );

	$affiliate_link_data = unserialize( get_post_meta( types_render_field( 'affiliate-link-id' ), 'thirstyData', true ) );
	$hosting_main_url = parse_url( $affiliate_link_data['linkurl'] );
	?>

	<article id="host-<?php the_ID(); ?>" class="host-item host-long-info post-<?php the_ID(); ?> host type-host status-publish hentry feature-one-click-install feature-unlimited-bandwidth feature-unlimited-disk-space location-united-states payment-credit-card payment-paypal" role="article">
		<div class="inner-host-item clearfix">
			<div class="row">
				<div class="col-md-12 host-col">

					<div class="host-thumb host-col-item">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo types_render_field( 'logo', array( 'output' => 'raw' ) ); ?>" class="img-responsive"></a>
					</div>

					<div class="host-info host-col-item">
						<h3 class="host-title" itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<p class="price"><small><?php _e( 'Price', 'hosting-reviews' ); ?>: </small><span><?php echo types_render_field( 'price' ); ?></span></p>
						<p class="uptime"><small><?php _e( 'Uptime', 'hosting-reviews' ); ?>: </small><span><?php echo types_render_field( 'uptime' ); ?>%</span></p>
						<p class="page-load"><small><?php _e( 'Speed', 'hosting-reviews' ); ?>: </small><span><?php echo types_render_field( 'speed' ); ?>ms</span></p>
					</div>

					<div class="rating-circles">
						<div class="rating-support text-center host-col-item">
							<div id="rating-support-progress" class="rating">
								<span class="rating-number"></span>
							</div>
							<span class="rating-label"><?php _e( 'Support', 'hosting-reviews' ); ?></span>
						</div>
						<div class="rating-uptime text-center host-col-item">
							<div id="rating-uptime-progress" class="rating">
								<span class="rating-number"></span>
							</div>
							<span class="rating-label"><?php _e( 'Uptime', 'hosting-reviews' ); ?></span>
						</div>
						<div class="rating-features text-center host-col-item">
							<div id="rating-features-progress" class="rating">
								<span class="rating-number"></span>
							</div>
							<span class="rating-label"><?php _e( 'Price', 'hosting-reviews' ); ?></span>
						</div>
						<div class="rating-page-load text-center host-col-item">
							<div id="rating-page-load-progress" class="rating">
								<span class="rating-number"></span>
							</div>
							<span class="rating-label"><?php _e( 'Speed', 'hosting-reviews' ); ?></span>
						</div>
					</div>

					<div class="rating-overall-container host-col-item">
						<div class="rating-overall text-center">
							<span class="rating-number"></span>
							<span class="rating-label"><?php _e( 'Overall', 'hosting-reviews' ); ?></span>
							<a class="btn btn-sm btn-primary" href="<?php the_permalink(); ?>"><?php _e( 'Read Review', 'hosting-reviews' ); ?></a>
							<a class="btn btn-sm btn-primary" href="<?php echo get_post_permalink( types_render_field( 'affiliate-link-id' ) ); ?>" target="_blank" rel="nofollow"><?php echo $hosting_main_url['host']; ?></a>
						</div>
					</div>
					<script>
						csize = 80;
						cthick = 6;

						pval = <?php echo types_render_field( 'support-circle' ); ?>;
						circleColor = getColor(pval);
						jQuery('.host-long-info#host-<?php the_ID(); ?> #rating-support-progress').circleProgress({
							value: <?php echo types_render_field( 'support-circle' ); ?>,
							size: csize,
							thickness: cthick,
							lineCap: "round",
							startAngle: -Math.PI / 4 * 6,
							fill: {
				            // gradient: ["#f284ba", "#c63988"]
				            color: circleColor
				        }
				    }).on({
				    	'circle-animation-progress': function(event, progress, stepValue) {
				    		if(stepValue >= 1.0) {
				    			var rateValue = 10;
				    		} else {
				    			var rateValue = String((stepValue * 10).toFixed(1));
				    		}
				    		jQuery(this).find('.rating-number').text(rateValue);
				    	},
				    	'circle-animation-end': function(event) {
				    		pval = <?php echo types_render_field( 'support-circle' ); ?>;
				    		circleColor = getColor(pval);
				    		rnumber = jQuery(this).find('.rating-number');
				    		rnumber.css('color', circleColor);
				    	}
				    });


				    pval = <?php echo types_render_field( 'uptime-circle' ); ?>;
				    circleColor = getColor(pval);
				    jQuery('.host-long-info#host-<?php the_ID(); ?> #rating-uptime-progress').circleProgress({
				    	value: <?php echo types_render_field( 'uptime-circle' ); ?>,
				    	size: csize,
				    	thickness: cthick,
				    	lineCap: "round",
				    	startAngle: -Math.PI / 4 * 6,
				    	fill: {
				    		color: circleColor
				    	}
				    }).on({
				    	'circle-animation-progress': function(event, progress, stepValue) {
				    		if(stepValue >= 1.0) {
				    			var rateValue = 10;
				    		} else {
				    			var rateValue = String((stepValue * 10).toFixed(1));
				    		}
				    		jQuery(this).find('.rating-number').text(rateValue);
				    	},
				    	'circle-animation-end': function(event) {
				    		pval = <?php echo types_render_field( 'uptime-circle' ); ?>;
				    		circleColor = getColor(pval);
				    		rnumber = jQuery(this).find('.rating-number');
				    		rnumber.css('color', circleColor);
				    	}
				    });


				    pval = <?php echo types_render_field( 'price-circle' ); ?>;
				    circleColor = getColor(pval);
				    jQuery('.host-long-info#host-<?php the_ID(); ?> #rating-features-progress').circleProgress({
				    	value: <?php echo types_render_field( 'price-circle' ); ?>,
				    	size: csize,
				    	thickness: cthick,
				    	lineCap: "round",
				    	startAngle: -Math.PI / 4 * 6,
				    	fill: {
				    		color: circleColor
				    	}
				    }).on({
				    	'circle-animation-progress': function(event, progress, stepValue) {
				    		if(stepValue >= 1.0) {
				    			var rateValue = 10;
				    		} else {
				    			var rateValue = String((stepValue * 10).toFixed(1));
				    		}
				    		jQuery(this).find('.rating-number').text(rateValue);
				    	},
				    	'circle-animation-end': function(event) {
				    		pval = <?php echo types_render_field( 'price-circle' ); ?>;
				    		circleColor = getColor(pval);
				    		rnumber = jQuery(this).find('.rating-number');
				    		rnumber.css('color', circleColor);
				    	}
				    });


				    pval = <?php echo types_render_field( 'speed-circle' ); ?>;
				    circleColor = getColor(pval);
				    jQuery('.host-long-info#host-<?php the_ID(); ?> #rating-page-load-progress').circleProgress({
				    	value: <?php echo types_render_field( 'speed-circle' ); ?>,
				    	size: csize,
				    	thickness: cthick,
				    	lineCap: "round",
				    	startAngle: -Math.PI / 4 * 6,
				    	fill: {
				    		color: circleColor
				    	}
				    }).on({
				    	'circle-animation-progress': function(event, progress, stepValue) {
				    		if(stepValue >= 1.0) {
				    			var rateValue = 10;
				    		} else {
				    			var rateValue = String((stepValue * 10).toFixed(1));
				    		}
				    		jQuery(this).find('.rating-number').text(rateValue);
				    	},
				    	'circle-animation-end': function(event) {
				    		pval = <?php echo types_render_field( 'speed-circle' ); ?>;
				    		circleColor = getColor(pval);
				    		rnumber = jQuery(this).find('.rating-number');
				    		rnumber.css('color', circleColor);
				    	}
				    });


				    pval = <?php echo types_render_field( 'overall-grand-total' ); ?>;
				    circleColor = getColor(pval);
				    jQuery('.host-long-info#host-<?php the_ID(); ?> .rating-number').each(function(){
				    	if(pval >= 1.0) {
				    		var rateValue = 10;
				    	} else {
				    		var rateValue = String((pval * 10).toFixed(1));
				    	}
				    	jQuery(this).text(rateValue);
				    	jQuery('.host-long-info#host-<?php the_ID(); ?> .rating-overall-container').css('backgroundColor', circleColor);
				    });

				</script>
			</div>

		</div>
	</div>

</article>

<?php

do_action( 'genesis_after_entry' );

		endwhile; //* end of one post
		do_action( 'genesis_after_endwhile' );

	else : //* if no posts exist
	do_action( 'genesis_loop_else' );
	endif; //* end loop

}


/*
 * Get below content.
 */
add_action( 'genesis_after_content', 'p_whr_text_below_hostings_table' );
function p_whr_text_below_hostings_table( ) {
	$post = get_post( 129 ); ?>
	<div class="hosting-info">
		<h2><?php echo $post->post_title; ?></h2>
		<?php echo $post->post_content; ?>
	</div>
	<?php }


	genesis();
