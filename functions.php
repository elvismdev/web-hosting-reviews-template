<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Hosting Reviews Theme' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '2.2.2' );

//* Enqueue Google Fonts
add_action( 'wp_enqueue_scripts', 'genesis_sample_google_fonts' );
function genesis_sample_google_fonts() {

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Lato:300,400,700', array(), CHILD_THEME_VERSION );

}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( 'headings', 'drop-down-menu',  'search-form', 'skip-links', 'rems' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for 4-column footer widgets
add_theme_support( 'genesis-footer-widgets', 4 );

/**
 * Load Shortcodes
 */
require_once 'inc/shortcodes.php';

//* Remove 'You are here' from the front of breadcrumb trail
add_filter( 'genesis_breadcrumb_args', 'p_whr_breadcrumb' );
function p_whr_breadcrumb( $args ) {
	$args['prefix'] = sprintf( '<div %s><div class="container">', genesis_attr( 'breadcrumb' ) );
	$args['suffix'] = ' ' . __( 'Review', 'hosting-reviews' ) . '</div></div>';
	$args['sep'] = ' > ';
	$args['labels']['prefix'] = '';
	return $args;
}

/**
 * Include Extra Assets.
 */
add_action( 'wp_enqueue_scripts', 'p_whr_add_extra_assets', 1 );
function p_whr_add_extra_assets(  ) {
	wp_enqueue_style( 'p-fontawesome', get_stylesheet_directory_uri() . '/assets/vendor/font-awesome/css/font-awesome.min.css' );
	wp_enqueue_style( 'p-bootstrap', get_stylesheet_directory_uri() . '/assets/vendor/bootstrap/dist/css/bootstrap.min.css' );
	// Custom JS file for misc.
	wp_enqueue_script( 'p-custom-scripts', get_stylesheet_directory_uri() . '/assets/js/hosting-reviews.js', array( 'jquery' ), null );
	// jQuery Circle Progress
	wp_enqueue_script( 'p-jquery-circle-progress', get_stylesheet_directory_uri() . '/assets/vendor/jquery-circle-progress/dist/circle-progress.js', array( 'jquery' ), '1.1.3' );
}


/*
 * Order by overall grand total.
 */
add_action( 'pre_get_posts', 'p_whr_order_by_grand_total' );
function p_whr_order_by_grand_total( $query ) {
	if ( is_admin() ) {
		return;
	}

	if ( is_post_type_archive( 'hosting-review' ) && $query->is_main_query() ) {
		$query->set('meta_key', "wpcf-overall-grand-total");
		$query->set('orderby', 'meta_value_num');
		$query->set('order', 'DESC');
	}

}


/*
 * Add excerpts to pages.
 */
add_action( 'init', 'p_whr_add_excerpts_to_pages' );
function p_whr_add_excerpts_to_pages() {
	add_post_type_support( 'page', 'excerpt' );
}


/*
 * Customize the entire footer
 */
remove_action( 'genesis_footer', 'genesis_do_footer' );
add_action( 'genesis_footer', 'p_whr_do_footer_links' );
function p_whr_do_footer_links() {

	$page_about = 182;
	$page_contact = 184;
	$page_privacy_policy = 187;
	$page_resources = 189;
	$page_disclosure = 191;
	$page_submit_review = 220;

	//* Build the text strings. Includes shortcodes
	$backtotop_text = '[footer_backtotop]';
	$creds_text     = sprintf( '<a href="%s">%s</a> &#x0007C; <a href="%s">%s</a> &#x0007C; <a href="%s">%s</a> &#x0007C; <a href="%s">%s</a> &#x0007C; <a href="%s">%s</a> &#x0007C; <a href="%s">%s</a> &#x0007C; [footer_copyright] &#x0007C; <a href="%s">%s</a> &#x0007C; %s',
		get_permalink( $page_about ),
		get_the_title( $page_about ),
		get_permalink( $page_contact ),
		get_the_title( $page_contact ),
		get_permalink( $page_submit_review ),
		get_the_title( $page_submit_review ),
		get_permalink( $page_privacy_policy ),
		get_the_title( $page_privacy_policy ),
		get_permalink( $page_resources ),
		get_the_title( $page_resources ),
		get_permalink( $page_disclosure ),
		get_the_title( $page_disclosure ),
		get_bloginfo( 'url' ),
		get_bloginfo( 'name' ),
		__( 'All Rights Reserved', 'hosting-reviews' )
		);

	//* Filter the text strings
	$backtotop_text = apply_filters( 'genesis_footer_backtotop_text', $backtotop_text );
	$creds_text     = apply_filters( 'genesis_footer_creds_text', $creds_text );

	$backtotop = $backtotop_text ? sprintf( '<div class="gototop"><p>%s</p></div>', $backtotop_text ) : '';
	$creds     = $creds_text ? sprintf( '<div class="creds"><p>%s</p></div>', $creds_text ) : '';

	$output = $backtotop . $creds;

	//* Only use credits if HTML5
	if ( genesis_html5() )
		$output = '<p>' . $creds_text . '</p>';

	echo apply_filters( 'genesis_footer_output', $output, $backtotop_text, $creds_text );

}

/*
 * Section title below sticky header.
 */
add_action( 'genesis_after_header', 'p_whr_section_title' );
function p_whr_section_title() {
	if ( is_archive() || is_singular( 'hosting-review' ) ) { ?>
	<section id="section-title">
		<div class="wrap">
			<div class="container">
				<h1 class="title"><?php echo ( is_archive() ) ? get_the_archive_title() : get_the_title() . " " . __( 'Review', 'hosting-reviews' ); ?></h1>
				<small class="meta"><?php the_excerpt(); ?></small>
			</div>
		</div>
	</section>
	<?php }
}


/*
 * Reposition Breadcrumbs
 */
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );
add_action( 'genesis_after_header', 'genesis_do_breadcrumbs' );
